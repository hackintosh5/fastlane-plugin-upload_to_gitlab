require 'fastlane_core/ui/ui'

module Fastlane
  UI = FastlaneCore::UI unless Fastlane.const_defined?(:UI)

  module Helper
    class UploadToGitlabHelper
      def self.upload_file(http, dest_url, token, file_path)
        make_request(http, dest_url) do |url|
          req = Net::HTTP::Put.new(url)
          req["Authorization"] = "Bearer #{token}"
          req["Transfer-Encoding"] = "Chunked"
          req.body_stream = file_path
          req
        end
      end

      def self.create_release(http, base_url, project_id, token, tag, assets_links)
        make_request(http, "#{base_url}projects/#{project_id}/releases/") do |url|
          req = Net::HTTP::Post.new(url)
          req["Authorization"] = "Bearer #{token}"
          req["Content-Type"] = "application/json"
          req.body = JSON[{ tag_name: tag, ref: "", name: tag, assets: { links: assets_links } }]
          req
        end
        nil
      end

      def self.make_request(http, url, &block)
        req = yield(url)
        res = http.request(req)
        case res
        when Net::HTTPSuccess
          url
        when Net::HTTPRedirection
          make_request(http, res["Location"], &block)
        else
          UI.crash!("Error making request #{req.method} #{req.uri} #{req.path} due to #{res.inspect}")
        end
      end
    end
  end
end
