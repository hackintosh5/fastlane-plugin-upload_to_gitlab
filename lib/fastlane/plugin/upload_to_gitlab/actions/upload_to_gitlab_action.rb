require 'fastlane/action'
require_relative '../helper/upload_to_gitlab_helper'

module Fastlane
  module Actions
    class UploadToGitlabAction < Action
      def self.run(params)
        version = params[:version_name]
        project_id = params[:project_id]
        token = params[:token].strip
        base_url = params[:base_url]
        base_uri = URI(base_url)
        asset_paths = params[:asset_paths]
        Net::HTTP.start(base_uri.host, base_uri.port, use_ssl: base_uri.scheme == "https") do |http|
          assets_links = asset_paths.map do |asset_name, asset_path|
            release_url = "#{base_url}projects/#{project_id}/packages/generic/apk/#{version}/#{asset_name}"
            release_uri = URI(release_url)

            UI.verbose("Uploading asset to #{release_url}")
            final_url = Helper::UploadToGitlabHelper.upload_file(http, release_uri, token, asset_path)
            { name: asset_name, url: final_url, link_type: "package" }
          end

          Helper::UploadToGitlabHelper.create_release(http, base_url, project_id, token, version, assets_links)
          UI.success("Created GitLab release")
        end
      end

      def self.description
        "Fastlane plugin to upload releases to GitLab"
      end

      def self.authors
        ["Hackintosh 5"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "Provides a quick and easy way to upload tagged releases to GitLab"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :project_id,
                                       env_name: "GITLAB_PROJECT_ID",
                                       description: "The project ID where releases should be pushed to",
                                       optional: false,
                                       type: Integer),
          FastlaneCore::ConfigItem.new(key: :token,
                                       env_name: "GITLAB_TOKEN",
                                       description: "The GitLab Bearer authentication token",
                                       code_gen_sensitive: true,
                                       optional: false,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :base_url,
                                       env_name: "GITLAB_BASE_URL",
                                       description: "The base URL of the GitLab API (including the protocol and trailing slash)",
                                       optional: true,
                                       default_value: "https://gitlab.com/api/v4/",
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :version_name,
                                       env_name: "GITLAB_VERSION_NAME",
                                       description: "The version name to use for the asset upload",
                                       optional: false,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :asset_paths,
                                       env_name: "GITLAB_ASSET_PATHS",
                                       description: "A hash of filename: filepath",
                                       optional: false,
                                       type: Hash)
        ]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
